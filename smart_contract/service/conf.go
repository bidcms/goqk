package service

import (
	"log"
	"os"
	"strconv"

	"github.com/joho/godotenv"
)

// SmartContractConfig type
type SmartContractConfig struct {
	Port int
}

// Config type
var Config SmartContractConfig

// LoadSmartContractConfig function
func LoadSmartContractConfig() *SmartContractConfig {
	var config SmartContractConfig
	err := godotenv.Load()
	if err != nil {
		log.Fatal(err)
		return nil
	}

	if size, err := strconv.Atoi(os.Getenv("SmartContract.Port")); err == nil {
		config.Port = size
	} else {
		config.Port = 18004
	}
	return &config
}
