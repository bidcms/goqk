package service

// Contract - 智能合约接口
type Contract interface {
	Init() error
	Query(QureyRequest) (QureyResponse, error)
	Invoke(InvokeRequest) (InvokeResponse, error)
}

// QureyRequest - 查询请求数据，需要细化查询信息
type QureyRequest struct {
	Data string
}

// QureyResponse - 查询应答数据，需要细化结果信息
type QureyResponse struct {
	Data string
}

// InvokeRequest - 查询请求数据，需要细化查询信息
type InvokeRequest struct {
	Data string
}

// InvokeResponse - 查询应答数据，需要细化结果信息
type InvokeResponse struct {
	Data string
}
