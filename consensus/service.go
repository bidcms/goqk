package consensus

import (
	"context"
	"goqk/core/p2p"
)

// Service type.
type Service interface {
	CommitTransaction(ctx context.Context, args *p2p.Args, reply *p2p.Reply)
	CommitBlock(ctx context.Context, args *p2p.Args, reply *p2p.Reply)
	AsynWorldState(ctx context.Context, args *p2p.Args, reply *p2p.Reply)
	AsynBlocks(ctx context.Context, args *p2p.Args, reply *p2p.Reply)
}
