package consensus

import (
	"goqk/blockchain"
	"goqk/excitation"
)

// Consensus type.
type Consensus interface {
	AppendBlock()
	Work(*blockchain.Ledger, *excitation.Beltline)
}
