package message

import "goqk/blockchain"

var (
	// Input - 接受到的区块
	Input []blockchain.Block
	// Output - 待发送的区块
	Output []blockchain.Block
	// Transactions 接收到的交易
	Transactions []blockchain.Transaction
	// 待发送的交易
	
)
