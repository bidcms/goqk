package excitation

import (
	"encoding/hex"
	"encoding/json"
)

// Beltline - 生产线，用于激励
type Beltline interface {
	Yield(owner string) (string, error)
}

// CoinBeltline - Coin生产线
type CoinBeltline struct {
	Count int
}

// Yield - 实现生产Coin方法，当前设定50个
func (coin *CoinBeltline) Yield(owner string) (string, error) {
	coin = new(CoinBeltline)
	coin.Count = 50

	result, err := json.Marshal(coin)
	if err != nil {
		return "", err
	}

	return hex.EncodeToString(result), nil
}
