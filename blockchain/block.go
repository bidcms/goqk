package blockchain

import (
	"encoding/json"
)

// Block - 区块，账本一页
type Block struct {
	Header BlockHeader
	Body   BlockBody
}

// BlockHeader - 区块头部信息
type BlockHeader struct {
	Version    byte
	Index      int
	Timestamp  string
	PrevHash   string
	MerkleRoot string
	Hash       string
	Nonce      int
	Validator  string
}

// BlockBody - 区块体，主要存储交易数据
type BlockBody struct {
	Trans map[string][]byte
}

// MarshalBlock - 序列化区块
func MarshalBlock(b Block) ([]byte, error) {
	return json.Marshal(b)
}

// UnmarshalBlock - 反序列化区块
func UnmarshalBlock(data []byte) Block {
	var block Block
	json.Unmarshal(data, &block)
	return block
}
