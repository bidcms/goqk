package blockchain

// BlockChainConfig - 配置
type BlockChainConfig struct {
	FileName string
	FileSize int
	HTTPPort int
}

// Config - 模块配置
var Config BlockChainConfig
