package blockchain

import (
	"encoding/json"
	"time"
)

// Transaction - 存储交易数据
type Transaction struct {
	Key       string
	Data      []byte
	Committer string
	Time      time.Time
	Signature string
}

// MarshalTransaction - 序列化
func MarshalTransaction(t Transaction) ([]byte, error) {
	return json.Marshal(t)
}

// UnmarshalTransaction - 反序列化
func UnmarshalTransaction(data []byte) Transaction {
	var tran Transaction
	json.Unmarshal(data, &tran)
	return tran
}
