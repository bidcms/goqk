package blockchain

import (
	"errors"
	"goqk/core/db"
	"log"
	"sync"
)

// var bucketTree *tree.BucketTree

// Ledger - 账本（不是分类账本）
type Ledger struct {
	// BDB      BoltDB
	db       db.DigestDB
	CurBlock Block
	mtx      *sync.Mutex
	p        Persistence
}

// InitializeLedger -初始化
func (l *Ledger) InitializeLedger() {
	l.db.Initialize()
}

// Persistence - 持久化
func (l *Ledger) Persistence() error {
	data, err := MarshalBlock(l.CurBlock)
	if err != nil {
		return err
	}
	// 将区块追加到区块文件中
	l.p.Save(data)
	// l.BDB.Add([]byte(l.CurBlock.Header.Hash), data)
	// 讲交易数据保存到交易数据库中，
	// 交易数据库也可以由区块文件推导出
	if !l.db.Connected {
		log.Println("couchdb数据库连接错误，交易信息未保存到数据库，但已写入区块中")
		return errors.New("couchdb数据库连接错误。")
	}
	for _, v := range l.CurBlock.Body.Trans {
		tran := UnmarshalTransaction(v)
		l.db.Save(db.Digest{
			Key:       tran.Signature,
			Committer: tran.Committer,
			Time:      tran.Time,
			Info:      ""})
	}
	return nil
}

// AppendBlock - 追加区块
func (l *Ledger) AppendBlock(block Block) error {
	l.CurBlock = block
	return l.Persistence()
}
