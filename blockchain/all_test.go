package blockchain

import (
	"bufio"
	"encoding/binary"
	"log"
	"os"
	"testing"
	"time"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

/**
* 判断文件是否存在  存在返回 true 不存在返回false
 */
func checkFileIsExist(filename string) bool {
	var exist = true
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		exist = false
	}
	return exist
}

func TestSave(t *testing.T) {
	// var wireteString = "测试n"
	// var filename = "./output1.txt"
	// var f *os.File
	// var err1 error
	// // /***************************** 第一种方式: 使用 io.WriteString 写入文件 ***********************************************/
	// if checkFileIsExist(filename) { //如果文件存在
	// 	// f, err1 = os.OpenFile(filename, os.O_APPEND, 0666) //打开文件
	// 	f, err1 = os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	// 	// log.Println(err1)
	// } else {
	// 	f, err1 = os.Create(filename) //创建文件
	// 	// fmt.Println("文件不存在")
	// }
	// check(err1)
	// n, err1 := io.WriteString(f, wireteString) //写入文件(字符串)
	// check(err1)
	// fmt.Printf("写入 %d 个字节n", n)

	/*****************************  第二种方式: 使用 ioutil.WriteFile 写入文件 ***********************************************/
	// var d1 = []byte(wireteString)
	// err2 := ioutil.WriteFile("./output2.dat", d1, 0666) //写入文件(字节数组)
	// check(err2)

	/*****************************  第三种方式:  使用 File(Write,WriteString) 写入文件 ***********************************************/
	// f, err3 := os.Create("./output3.txt") //创建文件
	// check(err3)
	// defer f.Close()
	// n2, err3 := f.Write(d1) //写入文件(字节数组)
	// check(err3)
	// fmt.Printf("写入 %d 个字节n", n2)
	// n3, err3 := f.WriteString("writesn") //写入文件(字节数组)
	// fmt.Printf("写入 %d 个字节n", n3)
	// f.Sync()
	/***************************** 第四种方式:  使用 bufio.NewWriter 写入文件 ***********************************************/
	// w := bufio.NewWriter(f) //创建新的 Writer 对象
	// r := bufio.NewReader(f)
	// n4, err3 := w.WriteString("bufferedn")
	// fmt.Printf("写入 %d 个字节n", n4)
	// w.Flush()
	// f.Close()
	// d1 := db.Digest{Key: "111", Time: time.Now(), Committer: "a"}
	// d2 := db.Digest{Key: "222", Time: time.Now()}
	// data1, _ := json.Marshal(d1)
	// data2, _ := json.Marshal(d2)
	// serialize(data1)
	// serialize(data2)
	// // f.Close()
	// var d3 db.Digest
	// data3, _ := deserialize()
	// json.Unmarshal(data3, &d3)
	// log.Println(d3.Key)
}

func getF() *os.File {
	var filename = "./blockchain.dat"
	var f *os.File
	var err1 error
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		//如果文件存在
		f, err1 = os.Create(filename)
		if err1 != nil {
			log.Fatal(err1)
		}
	} else {
		f, err1 = os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
		if err1 != nil {
			log.Fatal(err1)
		}
	}
	fi, err := f.Stat()
	if err != nil {
		log.Fatal(err)
	}
	if int(fi.Size()) >= Config.FileSize {
		os.Rename("./blockchain.dat", "./blockchain.dat."+time.Now().String())
		f, err1 = os.Create(filename)
		if err1 != nil {
			log.Fatal(err1)
		}
	}
	return f
}

func serialize(blocks ...[]byte) {
	f := getF()
	defer f.Close()
	for _, data := range blocks {
		len := uint32(len(data))
		log.Println(len)
		w := bufio.NewWriter(f)
		// 先写入block长度
		binary.Write(w, binary.BigEndian, len)
		// 写入block数据
		w.Write(data)
		w.Flush()
	}
}

func deserialize() (result [][]byte, err error) {
	var filename = "./blockchain.dat"
	var f *os.File
	f, err = os.OpenFile(filename, os.O_RDWR, os.ModeAppend)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	r := bufio.NewReader(f)
	var len1 uint32
	for {
		// 先读取block长度
		if err := binary.Read(r, binary.BigEndian, &len1); err != nil {
			break
		}
		// 读取block数据
		data := make([]byte, len1)
		if _, err := r.Read(data); err != nil {
			break
		} else {
			result = append(result, data)
		}
	}
	return result, nil
}
