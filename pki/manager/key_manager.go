package manager

import (
	"log"

	"github.com/gmsm/sm2"
)

// KeyManager - 秘钥管理
type KeyManager struct {
}

// GenerateKeyFile - 生成秘钥对
func (km *KeyManager) GenerateKeyFile(password string, privPath string, pubPath string) error {
	// 生成密钥对
	priv, err := sm2.GenerateKey()
	if err != nil {
		log.Println(err)
		return err
	}

	// 生成密钥文件
	ok, err := sm2.WritePrivateKeytoPem(privPath, priv, []byte(password))
	if !ok {
		log.Println(err)
		return err
	}

	pubKey := &priv.PublicKey
	// 生成公钥文件
	ok, err = sm2.WritePublicKeytoPem(pubPath, pubKey, nil)
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}

// GenerateSystemKeyFile - 生成系统秘钥对，主要是用于机构自己的秘钥
func (km *KeyManager) GenerateSystemKeyFile() {
	km.GenerateKeyFile(StoreConfig.SystemPassword, StoreConfig.SystemPrvKey, StoreConfig.SystemPubKey)
}
