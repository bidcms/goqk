package manager

import (
	"fmt"
	"io/ioutil"
	"testing"
)

func TestGenerateSystemKeyFile(t *testing.T) {
	var km KeyManager
	km.GenerateSystemKeyFile()
}

func BenchmarkGenerateSystemKeyFile(t *testing.B) {
	var km KeyManager
	km.GenerateSystemKeyFile()
}

func TestGenerateCert(t *testing.T) {
	var manager CertificateManager
	pubkey, _ := ioutil.ReadFile(StoreConfig.SystemPubKey)

	cert, _ := manager.GenerateClientCert("test", "rosefinch", pubkey)
	if len(cert) > 0 {
		fmt.Println("ok1")
	}

	err := manager.CheckSystemSignature(cert)
	if err != nil {
		fmt.Println("..................")
	}
}
