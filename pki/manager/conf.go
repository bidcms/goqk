package manager

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

// Config type.
type Config struct {
	StoreDirectory string
	Port           string
	SystemPrvKey   string
	SystemPubKey   string
	SystemPassword string
}

// LoadConfig - 读取配置
func LoadConfig() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal(err)
	}
	StoreConfig.StoreDirectory = os.Getenv("StoreDirectory")
	StoreConfig.Port = os.Getenv("Port")
	StoreConfig.SystemPrvKey = os.Getenv("SystemPrvKey")
	StoreConfig.SystemPubKey = os.Getenv("SystemPubKey")
	StoreConfig.SystemPassword = os.Getenv("SystemPassword")
}

// StoreConfig value.
var StoreConfig Config
