package main

import (
	"goqk/pki/manager"
	_ "goqk/pki/manager"
)

func main() {
	manager.LoadConfig()
	server := new(manager.RAServer)
	server.Run()
}
