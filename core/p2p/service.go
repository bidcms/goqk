package p2p

import (
	"context"
	"fmt"
)

// Args type
type Args1 struct {
	A int
	B int
}

// Reply type
type Reply1 struct {
	C int
}

// Arith type
type Arith int

// Mul function
func (t *Arith) Mul(ctx context.Context, args *Args1, reply *Reply1) error {
	reply.C = args.A * args.B
	fmt.Printf("call: %d * %d = %d\n", args.A, args.B, reply.C)
	return nil
}

// Add function
func (t *Arith) Add(ctx context.Context, args *Args1, reply *Reply1) error {
	reply.C = args.A + args.B
	fmt.Printf("call: %d + %d = %d\n", args.A, args.B, reply.C)
	return nil
}

// Say function
func (t *Arith) Say(ctx context.Context, args *string, reply *string) error {
	*reply = "hello " + *args
	return nil
}
