package p2p

// P2PConfig type.
type P2PConfig struct {
	Port    int
	Address *string
}

// Config 配置
var Config P2PConfig
