package p2p

// Args type.
type Args struct {
	Data string
}

// Reply type.
type Reply struct {
	Data string
}
