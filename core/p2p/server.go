package p2p

import (
	"context"
	"errors"
	"log"

	"github.com/smallnest/rpcx/protocol"
	"github.com/smallnest/rpcx/server"
)

// Server - 账本服务器
type Server struct {
	// config P2PConfig
}

// Run - 启动p2p服务器
func (ls *Server) Run(name string, rcvr interface{}, meta string) {
	// 客户端之间是对等的，如何能保证每两个客户端之间使用不同的秘钥呢？
	// cert, err := tls.LoadX509KeyPair("server.pem", "server.key")
	// if err != nil {
	// 	log.Print(err)
	// 	return
	// }

	// config := &tls.Config{Certificates: []tls.Certificate{cert}}

	// s := server.NewServer(server.WithTLSConfig(config))
	s := server.NewServer()
	s.RegisterName(name, rcvr, meta)
	// s.AuthFunc = ls.auth
	// fmt.Println(*Config.Address)
	// s.Serve("reuseport", *Config.Address)
	log.Println("RPCX服务器启动，正监听地址：" + *Config.Address)
	s.Serve("tcp", *Config.Address)
}

func (ls *Server) auth(ctx context.Context, req *protocol.Message, token string) error {
	if token == "rosefinch tGzv3JOkF0XG5Qx2TlKWIA" {
		return nil
	}

	return errors.New("invalid token")
}
