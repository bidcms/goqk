package events

// Event - 事件
type Event struct {
	Context interface{}
	Flag    string
	Sender  string
}

// Trigger - 触发器
type Trigger struct {
	Sender string
	Flag   string
	mtrig  Trig
}

// Trig - 回调
type Trig func(e Event)

var eventCollection []Trigger

// Subscribe - 注册监听
func Subscribe(sender string, flag string, trig Trig) {
	eventCollection = append(eventCollection, Trigger{Sender: sender, Flag: flag, mtrig: trig})
}

// Publish - 发布事件
func Publish(e Event) {
	for _, t := range eventCollection {
		if t.Flag == e.Flag && t.Sender == e.Sender {
			t.mtrig(e)
		}
	}
}

//go events.Publish(events.Event{Sender: "", Flag: "", Context: []byte("")})
