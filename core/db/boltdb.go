package db

import (
	"log"

	"github.com/boltdb/bolt"
)

// BoltDB - 用于存储区块
type BoltDB struct {
	// db *bolt.DB
}

// NewBoltDB function
func NewBoltDB() *BoltDB {
	return &BoltDB{}
}

// Add - 加入区块
func (bdb *BoltDB) Add(key, val []byte) error {
	db, err := bolt.Open("blockchain.db", 0600, nil)
	if err != nil {
		log.Println("boltdb创建失败")
		return err
	}
	defer db.Close()

	return db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte("blockchain"))
		if err != nil {
			return err
		}
		return b.Put(key, val)
	})
}

// Get - 获取区块
func (bdb *BoltDB) Get(key []byte) ([]byte, error) {
	db, err := bolt.Open("blog.db", 0600, nil)
	if err != nil {
		log.Println("boltdb创建失败")
		return nil, err
	}
	defer db.Close()

	var v []byte
	f := func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("blockchain"))
		v = b.Get(key)
		return nil
	}
	if err := db.View(f); err != nil {
		return nil, err
	}

	return v, nil
}

// Del - 删除区块
func (bdb *BoltDB) Del(key []byte) error {
	db, err := bolt.Open("blog.db", 0600, nil)
	if err != nil {
		log.Println("boltdb创建失败")
		return err
	}

	return db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte("blockchain"))
		if err != nil {
			return err
		}
		return b.Delete(key)
	})
}
