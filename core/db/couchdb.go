package db

import (
	"log"
	"time"

	couchdb "github.com/rhinoman/couchdb-go"
)

// Digest - 交易摘要信息
type Digest struct {
	Key       string
	Committer string
	Time      time.Time
	Info      string
}

// DigestDB - 交易存储数据库
type DigestDB struct {
	db        *couchdb.Database
	Connected bool
}

// Initialize - 初始化CouchDB数据库
// CouchDB安装在本地
// 后期可以考虑使用MongoDB代替
// CouchDB主要存储交易摘要信息，方便查询，可以由区块数据推导出数据库
func (d *DigestDB) Initialize() {
	var timeout = time.Duration(5000000 * time.Millisecond)
	conn, err := couchdb.NewConnection("127.0.0.1", 5984, timeout)
	if err != nil {
		log.Println(err)
		return
	}

	log.Println("建立CouchDB连接")
	auth := couchdb.BasicAuth{Username: "root", Password: "123456"}
	d.db = conn.SelectDB("digests", &auth)

	d.Connected = true
}

// Save - 保存文档
func (d *DigestDB) Save(doc Digest) error {
	_, err := d.db.Save(doc, doc.Key, "")
	if err != nil {
		log.Println(err)
		return err
	}
	log.Println("交易保存成功")
	return nil
}

// Get - 获取文档
func (d *DigestDB) Get(key string) Digest {
	dig := Digest{}
	d.db.Read(key, &dig, nil)
	return dig
}
