package tree

// TreeConfig - bucket-tree的配置
type TreeConfig struct {
	// bucket总容量
	Capacity int
	// 层级
	Level int
}

// Config type
var Config TreeConfig
