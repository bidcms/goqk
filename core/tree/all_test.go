package tree

import (
	"fmt"
	"goqk/core/common"
	"goqk/core/crytography"
	"testing"
	"time"
)

func TestTree(t *testing.T) {
	var tree BucketTree
	if err := tree.Build(); err != nil {
		fmt.Println(err.Error())
	}

	ent := &Entry{Owner: nil,
		TimeStamp: time.Now(),
		Data:      []byte("test")}
	ent.Hash = crytography.Hash(common.BytesCombine(ent.Data, common.Int64ToBytes(ent.TimeStamp.UnixNano())))
	bucket := tree.PushEntry(ent)

	fmt.Println(bucket.Hash)
	fmt.Println(tree.MerkleRoot.Hash)

}
