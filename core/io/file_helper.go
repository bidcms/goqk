package io

import (
	"bufio"
	"io/ioutil"
	"os"
	"path/filepath"
)

// WriteFile function
func WriteFile(filename string, data []byte) {
	var f *os.File
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		// 创建文件
		f, err = os.Create(filename)
	} else {
		// 打开文件
		f, err = os.OpenFile(filename, os.O_APPEND, 0666)
	}
	defer f.Close()

	// 创建新的 Writer 对象
	w := bufio.NewWriter(f)
	if _, err := w.Write(data); err != nil {
		return
	}
	w.Flush()
}

// ReadAll 读取文件内容
func ReadAll(filePth string) ([]byte, error) {
	f, err := os.Open(filePth)
	if err != nil {
		return nil, err
	}

	return ioutil.ReadAll(f)
}

// GetFileSize - 获取文件大小
func GetFileSize(filename string) int64 {
	var result int64
	filepath.Walk(filename, func(path string, f os.FileInfo, err error) error {
		result = f.Size()
		return nil
	})
	return result
}
