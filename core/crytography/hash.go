package crytography

import (
	"crypto/md5"
	"encoding/hex"

	"github.com/gmsm/sm3"
)

// Hash sm3.
func Hash(input []byte) string {
	tmp := sm3.Sm3Sum(input)
	return hex.EncodeToString(tmp)
}

// Md5Hash hash .
func Md5Hash(input []byte) string {
	hash := md5.New()
	hash.Write(input)
	return hex.EncodeToString(hash.Sum(nil))
}
