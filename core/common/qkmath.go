package common

import "math"

// Sqrt - 开方运算
func Sqrt(d1, d2 float64) float64 {
	d2 = 1 / d2
	return math.Pow(d1, d2)
}
