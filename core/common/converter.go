package common

import (
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"strconv"
)

//BytesCombine 多个[]byte数组合并成一个[]byte
func BytesCombine(pBytes ...[]byte) []byte {
	return bytes.Join(pBytes, []byte(""))
}

// Int64ToBytes function
func Int64ToBytes(i int64) []byte {
	var buf = make([]byte, 8)
	binary.BigEndian.PutUint64(buf, uint64(i))
	return buf
}

// BytesToInt64 function
func BytesToInt64(buf []byte) int64 {
	return int64(binary.BigEndian.Uint64(buf))
}

// BytesToHexString function
func BytesToHexString(src []byte) string {
	return hex.EncodeToString(src)
}

// IntToString function
func IntToString(src int) string {
	return strconv.Itoa(src)
}
