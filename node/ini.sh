#!/bin/bash

cd ~/go/src/goqk/node/
go build main.go
cp main ~/block_test/node1/
cp main ~/block_test/node2/
rm -rf ~/block_test/node1/block/*
rm -rf ~/block_test/node2/block/*
rm -rf ~/go/src/goqk/node/block/*
