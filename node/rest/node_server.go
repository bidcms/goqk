package rest

import (
	conf "goqk/node/config"
	"io"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/mux"
)

// Message type
type Message struct {
	CommonName   string
	Organization string
	PubKeyData   string
}

var mutex = &sync.Mutex{}

// NodeServer type.
type NodeServer struct {
}

// Run function
func (bs *NodeServer) Run() error {
	mux := bs.makeMuxRouter()
	httpPort := conf.Config.Port
	log.Println("节点服务器正在监听端口:", httpPort)
	s := &http.Server{
		Addr:           ":" + string(httpPort),
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	if err := s.ListenAndServe(); err != nil {
		return err
	}

	return nil
}

// create handlers
func (bs *NodeServer) makeMuxRouter() http.Handler {
	muxRouter := mux.NewRouter()
	muxRouter.HandleFunc("/commit_transaction", bs.handleCommitTransaction).Methods("GET")
	// muxRouter.HandleFunc("/registry", bs.handleRegistry).Methods("POST")
	return muxRouter
}

func (bs *NodeServer) handleCommitTransaction(w http.ResponseWriter, r *http.Request) {
	// pemName := "pubkey.pem"
	// w.Header().Set("Content-Type", "application/octet-stream")
	// w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=\"%s\"", pemName))
	// bytes := bs.manager.GetSystemPublicKey()
	// io.WriteString(w, string(bytes))

	// bytes, err := json.MarshalIndent(Blockchain, "", "  ")
	// if err != nil {
	// 	http.Error(w, err.Error(), http.StatusInternalServerError)
	// 	return
	// }
	io.WriteString(w, "")
}
