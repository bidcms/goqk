

[libp2p](https://libp2p.io/)
https://github.com/libp2p/go-libp2p
[mux](https://github.com/gorilla/mux)
https://en.wikipedia.org/wiki/MUX
[rpcx](http://rpcx.site/)
https://github.com/smallnest/rpcx
[libopenstorage/gossip](https://github.com/libopenstorage/gossip)
https://github.com/libopenstorage/gossip
[memberlist]https://github.com/hashicorp/memberlist
https://github.com/hashicorp/memberlist

go get github.com/davecgh/go-spew/spew
go get github.com/gorilla/mux
go get github.com/joho/godotenv
go get -u -v github.com/smallnest/rpcx/...
go get -u -fix github.com/libopenstorage/gossip
go get -u -fix github.com/hashicorp/memberlist